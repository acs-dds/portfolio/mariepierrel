<?php

function encode($character) {
	$c = ord($character);
	if (!($c % 3)) {
		return chr($c * 2 + 15);
	} else return chr($c);
}
echo encode('f');

//cette fonction utilise un caractère transfomé en ascii qu'on manipule dans des operations