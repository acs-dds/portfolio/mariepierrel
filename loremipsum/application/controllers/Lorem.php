<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lorem extends CI_Controller {

	public function index() 
	{
		$this->load->model('lorem_model');
		
		$data['para'] = $this->lorem_model->genererParagraphe();

		$this->load->view('templates/header', $data);
		$this->load->view('paragraphe', $data);
		$this->load->view('templates/footer', $data);
	}

	public function generer() 
	{
			$nb = $this->input->post('nb');
			$themes = $this->input->post('themes');

			$this->load->model('lorem_model');

			foreach ($themes as $theme) {
				$this->lorem_model->loadTheme($theme);
			}

			$data['paras'] = $this->lorem_model->genererParagraphes($nb);

			$this->load->view('templates/header', $data);
			$this->load->view('paragraphes', $data);
			$this->load->view('templates/footer', $data);

	}
}