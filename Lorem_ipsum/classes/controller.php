<?php

require_once __ DIR__.'/mapper.php';

class Controller () {
	private $mapper;

	public function __construct () {
		$this->mapper = new Mapper();
	}

	public function genererParagrapheAction () {
		return $this->mapper->genererParagraphe ();
	}

	// je veux d'abord générer un paragraphe avec un nombre de mots choisi par l'utiliosateurpublic function genererPlusieursParagraphes () {
		
	}
}