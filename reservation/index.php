<!DOCTYPE html>
<html>
<head>

    <link rel="icon" type="image/png" href="favicon.png" />
    <title>reZa | Réservation de Salle</title>
        <link rel="stylesheet" type="text/css" href="https://fullcalendar.io/css/base.css" />

        <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="stylemp.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css">

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>
        <script type="text/javascript" src="ajax.js"></script>
        <script type="text/javascript">
            $(function(){
                $("#calendar").fullCalendar({
                    header:{
                        left:"prev,next today",
                        center:"title",
                        right:"month,agendaWeek,agendaDay,listWeek"
                    },
                    eventLimit:true,
                    navLinks:true,
                    eventSources:[
                        {
                            url: 'event.php'
                        }
                    ]
                });
            });
        </script>
</head>

<body>

<!--    ** HEADER AVEC TITRE ** -->

    <header class="header" id="header">







    </header>


<!--    ** IMAGE ACCUEIL ** -->

    <h1 class="titre_accueil">Planning de réservation</h1>

    <div class="btn-accueil">

        <a href="#calendrier"><button class="btn btn-default btn-calendrier">Voir le calendrier</button></a>
        <a href="#reserver"><button class="btn btn-default btn-reservation">Réserver une salle</button></a>

    </div>

    <img class="img-accueil" src="accueil.jpg">

    <div class="show"></div>


<!--    ** CALENDRIER ** -->

        <div id="calendrier">
            <h2 class="titre_calendar">Calendrier</h2>
            <section class="calendar">
                <div id="body" class="section">
                    <div>
                        <div class="two-col">
                            <div class="content">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>


<!--    ** FORMULAIRE ** -->

        <div id="reserver" class="form">
          <div class="container">
            <div class="formulaire">
                <h2 class="titre_form">Formulaire de réservation de salle</h2>
            <div class="show"></div>
            <form>
                    <fieldset class="field1">
                            <label for="nom">Nom <em>*</em></label>
                            <input id="nom" name="nom" class="nom" placeholder="Entrez votre nom..."><br>
                            <label for="telephone" class="tel">Portable <em>*</em></label>
                            <input id="telephone" type="tel" placeholder="06-00-00-00-00"><br>
                            <label for="email" class="mail">Email <em>*</em></label>
                            <input id="email" type="email" placeholder="votre email"><br>
                    </fieldset>
                    <fieldset class="field2">
                        <label for="salle">Salle <em>*</em></label>
                        <select id="salle" name="salle" class="salle" placeholder="Choisir une salle">
                            <option value="1">Salle 1</option>
                            <option value="2">Salle 2</option>
                            <option value="3">Salle 3</option>
                        </select><br>
                            <label for="datedebut">Date Debut <em>*</em></label>
                            <input type="text" name="debut" value="" class="datetimepicker debut"/><br>
                            <label for="datefin">Date Fin <em>*</em></label>
                            <input type="text" name="fin" value="" class="datetimepicker fin"/>
                    </fieldset>
                    <p class="obligatoire"><i>Les champs marqués par </i><em>*</em> sont <em>obligatoires</em></p>
                  <p><input class="envoyer" type="submit" value="Soumettre"></p>
            </form>
        </div>
    </div>
</div>

<!--    ** INFOS ET TEXTE ** -->

<section class="informations">
        
        <h2 class="titre_infos">Etudiez tranquillement</h2>

    <div class="container">

        <p class="cite">

        Choisissez la salle qui vous convient, seul(e) ou à plusieurs, pour travailler, réfléchir et étudier dans le confort et le silence nécessaires.

        </p>

    </div>

</section>


<!--    ** FOOTER ** -->

    <div class="footer">
        
    <p class="reza">reZa | Réservation - Tous droits réservés - 2017</p>
    <p class="copy">BETA - &copy; ACS:DDS</p>


    </div>


        <script>
            $(".datetimepicker").datetimepicker({
                onGenerate:function(ct){
                    $(this).find(".xdsoft_date.xdsoft_weekend")
                        .addClass("xdsoft_disabled");
                },
                weekends:["01.01.2014","02.01.2014","03.01.2014","04.01.2014","05.01.2014","06.01.2014"],
                format: "Y-m-d H:i:00" 
            });
        </script>

</body>
</html>