<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tchat extends CI_Controller {

	public function index() 
	{
		$this->load->helper('url');

		$this->load->view('templates/header');
		$this->load->view('articletchat');
		$this->load->view('templates/footer');
	}

}