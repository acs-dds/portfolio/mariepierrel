<div class="row">
    <div class="small-6 large-expand columns">
		<?php foreach($succes as $s): ?>
		<article>
		<h2><?php echo $s->titre; ?></h2>
		<h3><?php echo $s->intitule; ?></h3>
		<?php if ($s->progression >= $s->objectif)?>
			<h4>Bingo, objectif atteint !</h4>
			<img src="<?php echo site_url('img/trophee1.svg') ; ?>" class="etoile">
		</article>
	</div>
		<?php endforeach; ?>
		<div class="progress [small-# large-#] [secondary alert success] [radius round]">
  		<span class="meter" style="width: [1 - 100]%"></span>
		</div>
</div>

