<form action="<?php echo site_url('signin'); ?>" method="post">
	<div class="row">
 		<div class="medium-6 columns">
    		<label for="signin-login">Nom d'utilisateur</label>
    		<input type="text" name="login" class="form-control" id="signin-login" placeholder="votre nom">
  		</div>
		<div class="medium-6 columns">
   		 	<label for="signin-mdp">Mot de passe</label>
    		<input type="password" name="mdp" class="form-control" id="signin-mdp" placeholder="votre mot de passe">
 	 	</div>
	 </div>
 	<div class="row">
  		<div class="medium-6 columns">
 			<button type="submit" class="success button">GO !</button>
 			<button type="button" class="hollow button secondary">Pas encore de compte ?</button>
   		</div>
	</div>
</form>
<?php if (isset($message)): ?><p class="error"><?php echo $message; ?></p><?php endif; ?>