<?php

class Utilisateur_model extends CI_Model {

        public function fetchUtilisateur($login, $mdp) {
                $req = "SELECT id, entreprise FROM utilisateur WHERE entreprise = ? AND mdp = ?;";
        	return $this->db->query($req, [$login, hash('sha512', "ok@/$mdp")])->row();
        }
}