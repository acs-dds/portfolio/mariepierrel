<?php

class Succes_model extends CI_Model {

		private $identreprise;

		public function setIdEntreprise($identreprise) {
			$this->identreprise = $identreprise;
		}

		private function ok() {
			return isset($this->identreprise);
		}

        public function fetchAllSucces() {
        		if ($this->ok())
                	return $this->db->query("SELECT id, titre, intitule, objectif, progression FROM succes WHERE idutilisateur = ? ORDER BY id ASC", [$this->identreprise])->result();
        }

        public function fetchSucces($id) {
        		if ($this->ok())
                	return $this->db->query("SELECT id, titre, intitule, objectif, progression FROM succes WHERE idutilisateur = ? AND id = ?", [$this->identreprise, $id])->row();
        }

        public function updateSucces($id, $progression) {
        		if ($this->ok())
                	$this->db->query("UPDATE succes SET progression = ? WHERE id = ? AND idutilisateur = ?;", [$progression, $id, $this->identreprise]);
        }
}