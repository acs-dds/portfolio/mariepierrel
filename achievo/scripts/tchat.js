$(document).ready(function() {
	var refresh = function() {
		$.post(
			'/chat/tchat/refresh',
			function (msg) {
				msg = JSON.parse(msg);
				for (var i = 0; i < msg.length; i++) {
					$('#message-list').append('<dt>' + msg[i].auteur + '</dt><dd>' + msg[i].contenu + '</dd>');
				}
			}
		);

		$.post(
			'/chat/tchat/getUsers',
			function (usr) {
				usr = JSON.parse(usr);
				var ul = $("#users-list");
				ul.html("");
				for (var i = 0; i < usr.length; i++) {
					ul.append('<li>' + usr[i].pseudo + '</li>');
				}
			}
		);
	};

	refresh();
	setInterval(refresh, 3500);

	$('#message-post').on('click', function() {
		if ($('#message-new').val().length == 0) return;

		$.post(
			'/chat/tchat/post',
			{'message': $('#message-new').val()},
			function (code) {
				if (parseInt(code) > 0) alert("Erreur lors de l'ajout du message");
				else {
					$('#message-new').val("");
					refresh();
				}
			}
		)
	});
});